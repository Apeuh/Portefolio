<body>
  <?php
session_start();
require("php/fonctions/connexion.php");
?>
    <!-- En-tête  avec logo, barre de recherche et navbar-->
    <header>
  	<nav class="navbar navbar-expand-md navbar-dark bg-dark w-100">
      <a class="navbar-brand" href="index.php"><img src="./assets/logo.png" alt="logo du site" height="60px"></a>
      <a href="panier.php" class="navbar-toggler navbar-expand-md"><img src="./assets/panier.png" alt="Panier"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample04">
        <ul class="navbar-nav mr-auto nav-fill w-100">
          <li class="nav-item active">
            <a class="nav-link" href="./index.php">TORTIPLAT Des plats à se tordre le ventre</a>
          </li>
          <li class="nav-item">
	          <form class="form-inline my-2 my-md-0">
	          	<input class="form-control" type="text" placeholder="Search">
        	  </form>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="menu.php?cat=all">Menu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="promo.php">Promos</a>
          </li>
          <li class="nav-item">
          <?php
            if (empty($_SESSION['user'])){
            echo "<a class='nav-link' href='./login.php'>Se connecter/s'inscrire</a>";
            }
            else echo "<a class='nav-link' href='compte.php'>Mon compte</a>";
          ?>
          </li>
          <li class="nav-item dropdown none">
            <a class="nav-link" href="panier.php">Panier</a>
          </li>
        </ul>
        
      </div>
    </nav>
    </header>