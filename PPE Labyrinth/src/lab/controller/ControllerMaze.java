package lab.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


import javax.swing.text.Position;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lab.util.Timeur;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import lab.vue.LabyrintheController;

/**
 * Creates a random maze, then solves it by finding a path from the
 * upper left corner to the lower right corner.  After doing
 * one maze, it waits a while then starts over by creating a
 * new random maze.  The point of the program is to visualize
 * the process.
 */
public class ControllerMaze implements Runnable  {
	
	private boolean test=false;
	
	public boolean execute(Canvas canva) throws InterruptedException {
		start(canva);
		return test;
	}
    //------------------------------------------------------------------------

    int[][] maze;   // Description of state of maze.  The value of maze[i][j]
                    // is one of the constants wallCode, pathcode, emptyCode,
                    // or visitedCode.  (Value can also be negative, temporarily,
                    // inside createMaze().)
                    //    A maze is made up of walls and corridors.  maze[i][j]
                    // is either part of a wall or part of a corridor.  A cell
                    // that is part of a corridor is represented by pathCode
                    // if it is part of the current path through the maze, by
                    // visitedCode if it has already been explored without finding
                    // a solution, and by emptyCode if it has not yet been explored.
    int[] position = {1,1};
    final static int backgroundCode = 0;
    final static int wallCode = 1;
    final static int pathCode = 2;
    final static int emptyCode = 3;
    final static int visitedCode = 4;
    final static int startCase = 5;
    final static int endCase = 6;
 // the canvas where the maze is drawn and which fills the whole window
    GraphicsContext g;  // graphics context for drawing on the canvas

    Color[] color;          // colors associated with the preceding 5 constants;
    int rows;          // number of rows of cells in maze, including a wall around edges
    int columns;       // number of columns of cells in maze, including a wall around edges
    private int blockSize;     // size of each cell
    int sleepTime = 4000;   // wait time after solving one maze before making another, in milliseconds
    int speedSleep = 20;    // short delay between steps in making and solving maze
    double initX;
    double initY;
    Label timeur;
    

    public  ControllerMaze(int columns,Label timeur) {
    	this.rows=columns;
    	this.columns=columns;
    	this.timeur=timeur;
    }
    
    public void start(Canvas canva) throws InterruptedException {
        color = new Color[] {
            Color.rgb(0,170,145),
            Color.rgb(0,170,145),
            Color.rgb(109,183,244),
            Color.WHITE,
            Color.rgb(200,200,200),
            Color.rgb(45,138,228),
            Color.rgb(205,92,92)
        };
        int Hmaze = rows * getBlockSize(); 
        int Wmaze = columns * getBlockSize(); 
        //init sont les positions 0 du labyrinthe, 
        //la fenetre fait 1280 par 800 donc la moiti� 
        //auquel on retranche la moiti� du labyrinthe        
        initX = 640-(Wmaze/2); 
        initY = 400-(Hmaze/2);
        System.out.println("initX: "+initX);
        System.out.println("initY: "+initY);
        maze = new int[rows][columns];
        g = canva.getGraphicsContext2D();
        g.setFill(color[backgroundCode]);
        g.fillRect(initX,initY-35,Wmaze,Hmaze);
        
        Thread runner = new Thread( this);
        runner.setDaemon(true);  // so thread won't stop program from ending
        runner.start();
    }
    
    void drawSquare( int row, int column, int colorCode ) {
            // Fill specified square of the grid with the
            // color specified by colorCode, which has to be
            // one of the constants emptyCode, wallCode, etc.
        Platform.runLater( () -> {
            g.setFill( color[colorCode] );
            int x = getBlockSize() * column;
            int y = getBlockSize() * row;
            g.fillRect(initX+x,initY+y-35,getBlockSize(),getBlockSize());
            
        });
    }
    @Override
    public void run() {
            // Run method for thread repeatedly makes a maze and then solves it.
            // Note that all access to the canvas by the thread is done using
            // Platform.runLater(), so that all drawing to the canvas actually
            // takes place on the application thread.
            try { Thread.sleep(1000); } // wait a bit before starting
            catch (InterruptedException e) { }
            makeMaze();
			Timeur timer = new Timeur();
			timer.timer(timeur);
			drawSquare(1,1,startCase);
			int endrow= rows-2;
			int endcol= columns-2;
	        drawSquare(endrow,endcol,endCase);
        
    }
    
    void makeMaze() {
            // Create a random maze.  The strategy is to start with
            // a grid of disconnected "rooms" separated by walls,
            // then look at each of the separating walls, in a random
            // order.  If tearing down a wall would not create a loop
            // in the maze, then tear it down.  Otherwise, leave it in place.
        int i,j;
        int emptyCt = 0; // number of rooms
        int wallCt = 0;  // number of walls
        int[] wallrow = new int[(rows*columns)/2];  // position of walls between rooms
        int[] wallcol = new int[(rows*columns)/2];
        for (i = 0; i<rows; i++)  // start with everything being a wall
            for (j = 0; j < columns; j++)
                maze[i][j] = wallCode;
        for (i = 1; i<rows-1; i += 2)  { // make a grid of empty rooms
            for (j = 1; j<columns-1; j += 2) {
                emptyCt++;
                maze[i][j] = -emptyCt;  // each room is represented by a different negative number
                if (i < rows-2) {  // record info about wall below this room
                    wallrow[wallCt] = i+1;
                    wallcol[wallCt] = j;
                    wallCt++;
                }
                if (j < columns-2) {  // record info about wall to right of this room
                    wallrow[wallCt] = i;
                    wallcol[wallCt] = j+1;
                    wallCt++;
                }
            }
        }
        Platform.runLater( () -> {
            g.setFill( color[emptyCode] );
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < columns; c++) {
                    if (maze[r][c] < 0)
                        g.fillRect( initX+c*getBlockSize(),initY-35+r*getBlockSize(), getBlockSize(), getBlockSize() );
                }
            }
        });
        synchronized(this) {
            try { wait(1000); }
            catch (InterruptedException e) { }
        }
        int r;
        for (i=wallCt-1; i>0; i--) {
            r = (int)(Math.random() * i);  // choose a wall randomly and maybe tear it down
            tearDown(wallrow[r],wallcol[r]);
            wallrow[r] = wallrow[i];
            wallcol[r] = wallcol[i];
        }
        for (i=1; i<rows-1; i++)  // replace negative values in maze[][] with emptyCode
            for (j=1; j<columns-1; j++)
                if (maze[i][j] < 0)
                    maze[i][j] = emptyCode;
        synchronized(this) {
            try { wait(1000); }
            catch (InterruptedException e) { }
        }
        
    }

    void tearDown(int row, int col) {
            // Tear down a wall, unless doing so will form a loop.  Tearing down a wall
            // joins two "rooms" into one "room".  (Rooms begin to look like corridors
            // as they grow.)  When a wall is torn down, the room codes on one side are
            // converted to match those on the other side, so all the cells in a room
            // have the same code.  Note that if the room codes on both sides of a
            // wall already have the same code, then tearing down that wall would 
            // create a loop, so the wall is left in place.
        if (row % 2 == 1 && maze[row][col-1] != maze[row][col+1]) {
            // row is odd; wall separates rooms horizontally
            fill(row, col-1, maze[row][col-1], maze[row][col+1]);
            maze[row][col] = maze[row][col+1];
            drawSquare(row,col,emptyCode);
            synchronized(this) {
                try { wait(speedSleep); }
                catch (InterruptedException e) { }
            }
        }
        else if (row % 2 == 0 && maze[row-1][col] != maze[row+1][col]) {
            // row is even; wall separates rooms vertically
            fill(row-1, col, maze[row-1][col], maze[row+1][col]);
            maze[row][col] = maze[row+1][col];
            drawSquare(row,col,emptyCode);
            synchronized(this) {
                try { wait(speedSleep); }
                catch (InterruptedException e) { }
            }
        }
    }

    void fill(int row, int col, int replace, int replaceWith) {
            // called by tearDown() to change "room codes".
            // (My algorithm really should have used the standard
            //  union/find data structure.)
        if (maze[row][col] == replace) {
            maze[row][col] = replaceWith;
            fill(row+1,col,replace,replaceWith);
            fill(row-1,col,replace,replaceWith);
            fill(row,col+1,replace,replaceWith);
            fill(row,col-1,replace,replaceWith);
        }
    }
    public void Victoire() throws IOException, SQLException {
    	LabyrintheController labycontrol = new LabyrintheController();
    	labycontrol.Victoire();
    }
    public void MoveMaze(String keymove) throws IOException, SQLException {
    	MoveCursor(position,keymove);
    }
    public int[] MoveCursor(int[] position,String keymove) throws IOException, SQLException {
    	int path=maze[1][1];
    	int row = position[0];
    	int col = position[1];
    	System.out.println(keymove);
    	
    	switch (keymove) {
	    	case "UP":
	    		if (maze[row-1][col]!=path) {
	    			System.out.println("Mur");
	    			return position;
	    		}
	    		else {
	    			drawSquare(row,col,visitedCode);
	    			row--;
	    			drawSquare(row,col,startCase);
	    			position[0]=row;
	    			position[1]=col;
	    			System.out.println(position[0]);
	    			System.out.println(position[1]);
	    			return position;
	    		}
	    	case"RIGHT":
	    		if (maze[row][col+1]!=path) {
	    			System.out.println("Mur");
	    			return position;
	    		}
	    		else {
	    			System.out.println("Chemin");
	    			drawSquare(row,col,visitedCode);
	    			col++;
	    			drawSquare(row,col,startCase);
	    			position[0]=row;
	    			position[1]=col;
	    			if (position[0]==rows-2 && position[1]==columns-2) {
	    	    		System.out.println("Victoire");
	    	    		Victoire();
	    	    		position[0]=1;
	    	    		position[1]=1;
	    	    		return position;
	    	    	}
	    			return position;
	    			
	    		}
	    	case"LEFT":
	    		if (maze[row][col-1]!=path) {
	    			System.out.println("Mur");
	    			return position;
	    		}
	    		else {
	    			System.out.println("Chemin");
	    			drawSquare(row,col,visitedCode);
	    			col--;
	    			drawSquare(row,col,startCase);
	    			position[0]=row;
	    			position[1]=col;
	    			return position;
	    		}
	    	case"DOWN":
	    		if (maze[row+1][col]!=path) {
	    			System.out.println("Mur");
	    			return position;
	    		}
	    		else {
	    			drawSquare(row,col,visitedCode);
	    			row++;
	    			drawSquare(row,col,startCase);
	    			position[0]=row;
	    			position[1]=col;
	    			if (position[0]==rows-2 && position[1]==columns-2) {
	    	    		System.out.println("Victoire");
	    	    		Victoire();
	    	    		position[0]=1;
	    	    		position[1]=1;
	    	    		return position;
	    	    	}
	    			return position;
	    		}
	    	default:
	    		return position;
	    	}
    }

	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	} 
}
    	
    



