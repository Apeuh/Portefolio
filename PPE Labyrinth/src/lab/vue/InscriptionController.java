package lab.vue;

import java.sql.SQLException;
import lab.Main;
import lab.classe.User;
import lab.controller.ControllerSoignant;
import lab.controller.ControllerUser;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class InscriptionController {
	ObservableList<String> choixList = FXCollections.observableArrayList("Patient","Soignant");
	@FXML
	private ChoiceBox choix;
	@FXML
	private TextField nom;
	@FXML
	private TextField prenom;
	@FXML
	private PasswordField password;
	@FXML
	private Label etat;
	@FXML
	private Button cancel;
	

		private Main main;
		 public void setMainSisi(Main main) {
		        this.main = main;}
		 
		 @FXML
		 public void initialize(){
			 choix.setItems(choixList);
		 }
		 
		 @FXML
		public void handleOK() throws SQLException {
				 if (!nom.getText().equals("") && !password.getText().equals("") && !prenom.getText().equals("")) {
					 String Nom=nom.getText().toString();
					 String Prenom=prenom.getText().toString();
					 String Login=prenom.getText().toString()+"."+nom.getText().toString();
					 String Password=User.hash(password.getText().toString());
					 if(choix.getValue().toString()=="Soignant") {
						 ControllerSoignant Controller = new ControllerSoignant();
						 Controller.Inscription(Nom, Prenom, Login.toLowerCase(), Password);
						 etat.setText("Inscription r�ussi");
						 handleCancel();
						 }
					 else {
						 ControllerUser Controller = new ControllerUser();
						 Controller.Inscription(Nom, Prenom, Login, Password);
						 etat.setText("Inscription r�ussi");
						 handleCancel();
					 }
				 }
				 else {
					etat.setText("Mail et password obligatoire/erron�"); 
				 }
				 
		 }
		
		@FXML
		public void handleCancel() {
			    Stage stage = (Stage) cancel.getScene().getWindow();
			    stage.close();

		}
}
