package lab.classe;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class User {
	private int id;
	private String nom;
	private String prenom;
	private String password;
	private String login;
	
	public User(int id, String nom, String prenom, String login, String password) {
		this.id=id;
		this.nom=nom;
		this.prenom=prenom;
		this.password=password;
		this.login=login;
	}
	
	public void setId(int id) {
		this.id= id;
	}
	
	public void setNom(String noms) {
		this.nom=noms;
	}
	public void setPrenom(String Prenom) {
		this.prenom= Prenom;
	}
	
	public String getPrenom() {
		return this.prenom;
	}
	
	public String getNom(){
		return this.nom;
	}
	
	public String toString() {
		return nom+" "+prenom;
	}
	
	public int getId() {
		return id;
	}
	
	public void setPassword(String password) {
		this.password=password;
	}
	
	public String getPassword() {
		return this.password;
	}

	public String getLogin() {
		return this.login;
	}
	
    public  static String hash(String password) {
    try {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        String salt = "Sardoche";
        String generatedPassword;
        if(password.isEmpty()) {
        	generatedPassword="";
        }
        else {
	        String passWithSalt = password + salt;
	        byte[] passBytes = passWithSalt.getBytes();
	        byte[] passHash = sha256.digest(passBytes);             
	        StringBuilder sb = new StringBuilder();
	        for(int i=0; i< passHash.length ;i++) {
	            sb.append(Integer.toString((passHash[i] & 0xff) + 0x100, 16).substring(1));         
	        }
	        generatedPassword = sb.toString();
        }
        return generatedPassword;
    } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }       
    return null;
}

}

