package lab.classe;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public class Partie {
	
	private String FullName;
	private String date;
	private String difficulte;
	private String score;
	private int id;
	Timestamp dtf;
	Time newScore;

	public Partie(String FullName,String date,String difficulte,String score) {
		this.FullName=FullName;
		this.date=date;
		this.difficulte=difficulte;
		this.score=score;
	}
	
	public Partie(int idpatient, Timestamp dtf, String diff, Time newScore) {
		this.id=idpatient;
		this.dtf=dtf;
		this.difficulte=diff;
		this.newScore=newScore;
	}

	public String toString(){
		return "FullName : "+FullName+" | Date : "+date+" | Difficulte : "+difficulte+" | Temps : "+score;
		
	}
	
	public int getId() {
		return this.id;
	}
	
	public Timestamp getDate() {
		return this.dtf;
	}
	
	public Time getScore() {
		return this.newScore;
	}
	
	public String getDifficulte() {
		return this.difficulte;
	}
}
