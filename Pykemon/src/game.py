import  pygame
import pytmx
import pyscroll
import map
from character import Player
from src.dialog import DialogBox


class Game :
    def __init__(self):
        # Création de la fenetre du jeu
        self.screen =pygame.display.set_mode((800, 600))
        name=pygame.display.set_caption("Pykemon")

        # Generer un joueur
        self.player = Player()
        self.map_manager= map.MapManager(self.screen, self.player)
        self.dialog_box= DialogBox()

        #Logo du jeu
        pygame.display.set_icon(self.player.get_image(0,0))

        panel=self.map_manager.get_object("panel")
        self.panel_rect=(panel.x,panel.y,panel.width,panel.height)

        # définir le rect d'entrée dans la maison
        #enter_house = tmx_data.get_object_by_name("porte")
        #self.enter_house_rect = pygame.Rect(enter_house.x, enter_house.y, enter_house.width, enter_house.height)

    def handle_input(self) :
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_z] :
            self.player.move_top()
        elif pressed[pygame.K_s] :
            self.player.move_bot()
        elif pressed[pygame.K_d] :
            self.player.move_right()
        elif pressed[pygame.K_q] :
            self.player.move_left()



    def update(self):
        self.map_manager.update()

    def Run(self) :

        clock = pygame.time.Clock()
        # boucle du jeu
        running = True
        while running :

            self.player.save_location()
            self.handle_input()
            self.update()
            self.map_manager.draw()
            self.dialog_box.render(self.screen)
            pygame.display.flip()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type ==pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        if self.player.rect.colliderect(self.panel_rect):
                            self.dialog_box.execute(["Va pas en bas, c'est nwar","En haut c'est mieux khoya"])
                        self.map_manager.check_npc_collision(self.dialog_box)

            clock.tick(60)

        pygame.quit()
