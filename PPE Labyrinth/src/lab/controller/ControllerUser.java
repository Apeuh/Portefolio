package lab.controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import lab.Main;
import lab.classe.*;
import lab.modele.ModeleSoignant;
import lab.modele.ModeleUser;

public class ControllerUser {
	public static ArrayList<User> Users = new ArrayList<User>();

	public void Login(String login, String password) throws SQLException {
		 ModeleUser Modele = new ModeleUser();
		 ResultSet rs =Modele.Login(login, password);
		 int id=0;
		 String nom=null;
		 String prenom = null;
		 String password1=null;
		 try {
			 while(rs.next()) {					
				 id=rs.getInt("idPatient");
		         nom=rs.getString("Nom");
		         prenom=rs.getString("Prenom");	
		         password1=rs.getString("password");
			}
			 if(nom!=null) {
				 User user= new User(id,nom,prenom,login,password1);
				 Users.add(user);
	}
		 }
		 catch(Exception e) {
	           e.printStackTrace();
	           }
		
	}

	public void Inscription(String nom, String prenom, String login, String password) throws SQLException {
		 ModeleUser Modele = new ModeleUser();
		 Modele.Inscription(nom, prenom, login, password);
	}
	
	public void Changement(String nom, String prenom, String password) throws SQLException {
		ModeleUser Modele = new ModeleUser();
		Modele.Changement(nom, prenom, password);
		Users.get(0).setNom(nom);
		Users.get(0).setPrenom(prenom);
		Users.get(0).setPassword(password);
	}

	public ArrayList<User> getListe() {
		return Users;
	}
	
	public void deconnexion() {
		ControllerUser.Users.clear();
		Main.FullScreen();
	}
}
