<?php
    require('php\class\Commande.php');
    require_once("php/db_connect.php");
    require_once("ControllerArticle.php");
class ControllerCommande{
    
    public $listecommandecuisine=array();
    public $histo=array();

    function CreateCommande(){
        $dbconnect=new db_connect;
        $req=$dbconnect->select('SELECT c.numero,round((p.price*co.quantity),2) as TotalPrice,c.date,concat(u.first_name," ",u.last_name) as FullName,"", concat(p.name_product," -> ", co.quantity," Fois") as Total, c.etatcommande FROM commande c
        inner join userr u on c.id_client=u.id_user
        inner join concerne co on c.id_commande=co.id_commande
        inner join products p on co.id_product=p.id_product');
        $req->execute();
        while ($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $data["numero"]= new Commande($data["numero"],$data['TotalPrice'],$data["date"],$data["FullName"],$data["Total"],$data["Total"],$data["etatcommande"]);
            array_push($this->listecommandecuisine,$data["numero"]);
        }
  }

  function addCommande(){
    $db=new db_connect;
    $Controller=new ControllerArticle;
    $Controller->CreateArticle();
    $db->select("INSERT INTO commande (numero, date, id_client) values (?,?,?);");
    $date=new DateTime('now');
    $db->execute([rand(),$date,$_SESSION["user"]->getID()]);
    foreach($_SESSION['Panier'] as $key=>$val){
        $db->select("INSERT INTO concerne (id_products,id_commande,quantity) values (?,(SELECT id_commande from commande order by id_commande DESC limit 1),?)");
        $id="";
        foreach($Controller->getListe() as $article){
            if($article->getName()==$key){
                $id=$article->getId();
            }
        }
        $db->execute([$id,$val]);
    })
  }

  function GetHisto(){
      $db=new db_connect;
    $ControllerArticle=new ControllerArticle;
    $ControllerArticle->CreateArticle();
      $req=$db->select("SELECT numero,p.price,c.date, p.name_product, co.quantity, c.etatcommande FROM `commande` c  
      inner join concerne co on co.id_commande=c.id_commande 
      inner join products p on co.id_product=p.id_product
      WHERE c.id_client=?
      GROUP BY numero");
      $req->execute([$_SESSION['user']->getId()]);
      $req2=$db->select("SELECT p.name_product,co.quantity  from products p
      inner join concerne co on co.id_product=p.id_product
      where id_commande=?");
      while($data=$req->fetch(PDO::FETCH_ASSOC)){
        $listeArticle=array();
        $total=0;
        $req2->execute([$data["numero"]]);
          while($data2=$req2->fetch(PDO::FETCH_ASSOC)){
            foreach($ControllerArticle->getListe() as $Article){
                if($Article->getName()==$data2["name_product"]){
                    $total+=$data2["quantity"]*$Article->getPrice();
                    $produit=$data2["quantity"].'-'.$data2["name_product"];
                    array_push($listeArticle,$produit);
                }
            }
          }
                $data["numero"]= new Commande($data["numero"],$total,$data["date"],$_SESSION['user']->getLastName(),$listeArticle,$data["etatcommande"]);
                array_push($this->histo,$data["numero"]);
      }
  }
    function getListe(){
        return $this->histo;
    }

    function DeleteCommande($idCommande){
        unset($listecommande[array_search($idCommande,$listecommande)]);

    }

    function getCommande($id=0){
        if($id!=0){

        }
        else {
            foreach ($this->listecommandecuisine as $val){
            $date=new DateTime('2021-12-20'/*'now'*/);    
                $val->toStringCuisine();
            }
        }
        
    }
    }
?>