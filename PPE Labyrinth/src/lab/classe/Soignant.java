package lab.classe;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class Soignant {
	private int id;
	private String nom;
	private String prenom;
	private String password;
	private String login;
	private ArrayList mail;

	public Soignant(int id, String nom, String prenom,String login, String password, ArrayList mail) {
		this.id=id;
		this.nom=nom;
		this.prenom=prenom;
		this.login=login;
		this.password=password;
		this.mail=mail;
	}

	public void setId(int id) {
		this.id= id;
	}

	public void setNom(String noms) {
		this.nom=noms;
	}
	public void setPrenom(String Prenom) {
		this.prenom= Prenom;
	}

	public String getPrenom() {
		return this.prenom;
	}
	
	public ArrayList getMail() {
		return this.mail;
	}

	public String getNom(){
		return this.nom;
	}

	public String toString() {
		return nom+" "+prenom;
	}
	
	public void setPassword(String password) {
		this.password=password;
	}
	
	public String getPassword() {
		return this.password;
	}

	public int getId() {
		return id;
	}
	
	public String getLogin() {
		return this.login;
	}
	
    public  static String hash(String password) {
    try {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        String salt = "Sardoche";
        String passWithSalt = password + salt;
        byte[] passBytes = passWithSalt.getBytes();
        byte[] passHash = sha256.digest(passBytes);             
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< passHash.length ;i++) {
            sb.append(Integer.toString((passHash[i] & 0xff) + 0x100, 16).substring(1));         
        }
        String generatedPassword = sb.toString();
        return generatedPassword;
    } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }       
    return null;
}
}
