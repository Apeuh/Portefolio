package lab.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lab.classe.Partie;
import lab.classe.Soignant;
import lab.modele.ModelePartie;
import lab.modele.ModeleSoignant;

public class ControllerPartie {

	public static ObservableList<String> resultats = FXCollections.observableArrayList();
	ModelePartie modele = new ModelePartie();
	
	
	public void LoadResult(String nom, String prenom) throws SQLException {
		ResultSet rs =modele.LoadResult(nom,prenom);
		String FullName;
		String date;
		String difficulte;
		String score;
		 try {
			 while(rs.next()) {					
				 FullName=rs.getString("FullName");
		         date=rs.getString("date");
		         difficulte=rs.getString("difficulte");	
		         score=rs.getString("score");
		         Partie partie = new Partie(FullName,date,difficulte,score);
		         resultats.add(partie.toString());
			}
	
		 }
		 catch(Exception e) {
	           e.printStackTrace();
	           }
	}
	
	public void addResult(Partie partie) throws SQLException {
		modele.addResult(partie);
	}
}
