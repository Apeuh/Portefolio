package lab.vue;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import lab.controller.ControllerPartie;
import lab.controller.ControllerSoignant;

public class ScoreController implements Initializable {
	
	@FXML
	private ListView<String> scroll;
	@FXML
	private TextField nom;
	@FXML
	private TextField prenom;
	
	@FXML
	public void LoadScore() throws SQLException {
		ControllerPartie controller = new ControllerPartie();
		String Nom=this.nom.getText().toString();
		String Prenom=this.prenom.getText().toString();
		controller.LoadResult(Nom,Prenom);
		scroll.getItems().setAll(ControllerPartie.resultats);
		ControllerPartie.resultats.clear();

	}
	
	public void disable() {
		if(ControllerSoignant.Soignants.isEmpty()) {
			nom.setDisable(true);
			prenom.setDisable(true);
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		disable();
	}
}
