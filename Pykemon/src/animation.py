import pygame

class AnimateSprite(pygame.sprite.Sprite) :

    def __init__(self,sprite_name,x,y):
        super().__init__()
        self.image = pygame.image.load(sprite_name)
        self.speed = 3
        self.clock=0
        self.animations = {
            'down': self.get_images(0),
            'left': self.get_images(32),
            'right': self.get_images(64),
            'up': self.get_images(96)
        }
        self.image = self.get_image(0, 0)
        self.rect = self.image.get_rect()
        self.position = [x, y]
        self.current_image = 0
        self.animation=False

        self.feet = pygame.Rect(0, 0, self.rect.width * 0.5, 12)
        self.old_position = self.position.copy()
        #definir une mathode qui anime le sprite

    def get_images(self,y):
        images=[]
        for i in range(0,3):
            x=i*32
            image=self.get_image(x,y)
            images.append(image)
        return images

    def get_image(self, x, y):
        image = pygame.Surface([32, 32])
        image.blit(self.image, (0, 0), (x, y, 32, 32))
        image.set_colorkey([0, 0, 0, 0])
        return image

    def start_animation(self):
        self.animation = True

    def move_back(self):
        self.position = self.old_position
        self.rect.topleft = self.position
        self.feet.midbottom = self.rect.midbottom


    def animate(self,name) :
        self.images=self.animations[name]
        if self.animation :
            self.clock+=self.speed*8
            if self.clock>=100 :
                rd = 1
                # passer à l'image suivante
                self.current_image += rd
                #vérifier si on a la fin de l'animation
                if self.current_image>= len(self.images) :
                    #remettre l'animation à 0
                    self.current_image =0
                #modifier l'image préédente à la suivante
                self.image = self.images[self.current_image]
                self.image.set_colorkey([0,0,0])
                self.animation=False
                self.clock=0
