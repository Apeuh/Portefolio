import pygame

import animation
class Entity(animation.AnimateSprite) :
    def __init__(self, name, x, y):
        super().__init__(f"./sprite/{name}.png", x, y)

    def save_location(self): self.old_position = self.position.copy()


    def move_right(self) :
        self.start_animation()
        self.animate("right")
        self.position[0]+=self.speed

    def move_left(self):
        self.start_animation()
        self.animate("left")
        self.position[0] -= self.speed

    def move_top(self):
        self.start_animation()
        self.animate("up")
        self.position[1] -= self.speed

    def move_bot(self):
        self.start_animation()
        self.animate("down")
        self.position[1] += self.speed


    def update(self):
        self.rect.topleft = self.position
        self.feet.midbottom = self.rect.midbottom

class Player(Entity):
    def __init__(self):
        super().__init__("player", 0, 0)


class NPC(Entity):
    def __init__(self,name,nb_point,dialog):
        super().__init__(name,0,0)
        self.dialog=dialog
        self.nb_point=nb_point
        self.name=name
        self.points=[]
        self.current_point=0
        self.speed=1.5

    def move(self):
        current_point=self.current_point
        target_point=self.current_point+1
        if target_point>=self.nb_point:
            target_point=0

        current_rect=self.points[current_point]
        target_rect=self.points[target_point]

        if current_rect.y<target_rect.y and abs(current_rect.x-target_rect.x)<3:
            self.move_bot()
        elif current_rect.y>target_rect.y and abs(current_rect.x-target_rect.x)<3:
            self.move_top()
        elif current_rect.x > target_rect.x and abs(current_rect.y - target_rect.y) < 3:
            self.move_left()
        elif current_rect.x < target_rect.x and abs(current_rect.y - target_rect.y) < 3:
            self.move_right()

        if self.rect.colliderect(target_rect):
            self.current_point=target_point



    def load_point(self,tmx_data):
        for num in range(1,self.nb_point+1):
            point=tmx_data.get_object_by_name(f"{self.name}_path{num}")
            rect=pygame.Rect(point.x,point.y,point.width,point.height)
            self.points.append(rect)

    def spawn_npc(self):
        location=self.points[self.current_point]
        self.position[0]=location.x
        self.position[1] = location.y
        self.save_location()



