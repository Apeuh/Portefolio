<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>Accept a payment</title>
    <meta name="description" content="A demo of a payment on Stripe" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" type="image/png" sizes="16x16" href="assets/logo.png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="Stripe/public/checkout.css" />
    <script src="https://js.stripe.com/v3/"></script>
    <script src="Stripe/public/checkout.js" defer></script>
  </head>
  <?php include_once('header.php');?>

  <div id='logo' class='bg-img bg-blk flexcenter'>
    <img src='./assets/Logolong800w.png'>
  </div>
  <div id=form>
    <!-- Display a payment form -->
    <form id="payment-form">
      <div id="payment-element">
        <!--Stripe.js injects the Payment Element-->
      </div>
      <button id="submit">
        <div class="spinner hidden" id="spinner"></div>
        <span id="button-text">Payer Maintenant</span>
      </button>
      <div id="payment-message" class="hidden"></div>
    </form>
<?php include_once('footer.php');?>
</div>