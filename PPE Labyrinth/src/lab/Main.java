package lab;
	
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lab.vue.LabyrintheController;
import lab.vue.RootLayoutController;

public class Main extends Application {
	
    public static Stage primaryStage;
    private static BorderPane rootLayout;
    private String path;
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        path="file:C:\\Users\\camil\\eclipse-workspace\\ppe-desktop\\PPE Labyrinth\\src\\assets/Minos.jpg";
        this.primaryStage.setTitle("Labyrinthe");
        this.primaryStage.getIcons().add(new Image(path));
        window(true);
        Stage secondStage=new Stage();
        secondStage.getIcons().add(new Image(path));
        showLogin(secondStage);
    }
    
    public static void window(Boolean x) {
        initRootLayout(x);
        showMaze();
    }
    
    public static void initRootLayout(Boolean x) {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("vue/SRootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            RootLayoutController controller = loader.getController();
            controller.Invisible(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showLogin(Stage primaryStage) {
    	try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("vue/Login.fxml"));
            Parent page = loader.load();   
            Scene scene = new Scene(page);
            primaryStage.setTitle("Login");
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();}
    }
    
    public static void showMaze() {
    	try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("vue/Labyrinthe.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();
            // Set person overview into the center of root layout.
            rootLayout.setCenter(personOverview);
        } catch(Exception e) {
            e.printStackTrace();}
    }
	public static void FullScreen() {
		if (Main.primaryStage.isMaximized()==true) {
			Main.primaryStage.setMaximized(false);
			Main.primaryStage.setMaximized(true);			
		}
		else {
		 Main.primaryStage.setMaximized(true);
		}
	}
	public static void main(String[] args) {
		launch(args);
	}
}
