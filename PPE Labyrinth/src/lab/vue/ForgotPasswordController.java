package lab.vue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import lab.util.dbConnection;

public class ForgotPasswordController {

	@FXML private TextField loginverrou;
	@FXML private CheckBox prefere;
	private dbConnection db= new dbConnection();
	
	public void SetLogin(String login) {
		loginverrou.setText(login);
		loginverrou.setEditable(false);
	}
	
	@FXML
	public void SendMail() throws SQLException {
		Connection connection= db.connect();
		PreparedStatement stat =null;
		ResultSet rs =null;
		String recipient = null;
		String sender = "mino.taur@hopital.com";
		String host = "127.0.0.1";
		Properties properties = System.getProperties();
		Session session = Session.getDefaultInstance(properties);
		properties.setProperty("mail.smtp.host", host);
		
		if(prefere.selectedProperty().getValue()) {
			 String sql = "SELECT mail,nom,prenom FROM asistant a natural join mail WHERE login = ? and Prefere=1";
				 stat=connection.prepareStatement(sql);
				 stat.setString(1, loginverrou.getText());
				 rs = stat.executeQuery();
				 recipient=rs.getString("Mail");
				    try
				    {
				       MimeMessage message = new MimeMessage(session);
				       message.setFrom(new InternetAddress(sender));
				       message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
				       message.setSubject("Mot de Passe oubli� pour MinoTaur");
				       message.setText("Bonjour ",rs.getString("nom")," ",rs.getString("prenom"),"\n Voici le lien pour r�initialiser le code : https://resetmdp.fr?login=",rs.getString("login"));
				       Transport.send(message);
				       System.out.println("Mail successfully sent");
				    }
				    catch (MessagingException mex)
				    {
				       mex.printStackTrace();
				    }
			
		}
		else {
			String sql = "SELECT mail,nom, prenom,login FROM asistant a natural join mail WHERE login = ?";
			 stat=connection.prepareStatement(sql);
			 stat.setString(1, loginverrou.getText());
			 rs = stat.executeQuery();
			 while(rs.next()) {
			 recipient+=rs.getString("Mail")+";";}
			    try
			    {
			       MimeMessage message = new MimeMessage(session);
			       message.setFrom(new InternetAddress(sender));
			       message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
			       message.setSubject("Mot de Passe oubli� pour MinoTaur");
			       message.setText("Bonjour ",rs.getString("nom")," ",rs.getString("prenom"),"\n Voici le lien pour r�initialiser le code : https://resetmdp.fr?login=",rs.getString("login"));
			       Transport.send(message);
			       System.out.println("Mail successfully sent");
			    }
			    catch (MessagingException mex)
			    {
			       mex.printStackTrace();
			    }
		}
	}
}
	