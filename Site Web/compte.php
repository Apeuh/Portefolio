<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tortiplat / Compte</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <?php require_once('php/db_connect.php');?>
  <script type="text/javascript" src="js\delOption.js"></script>
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/compte.css">
</head>
<?php 
require_once("php/controller/ControllerUser.php");
require_once("php/controller/ControllerArticle.php");
require_once("php/controller/ControllerCommande.php");
include_once('header.php'); 
$Control=new ControllerUser;
$ControlA=new ControllerAdress;
$controlart= new ControllerArticle;
$controlart->createArticle();

if (!isset($_SESSION['user'])){
    header('location: login.php');
}
//Check Modif
if(isset($_POST['change'])){
$Control->checkUser($_SESSION['user'],"nom",htmlentities($_POST['name']));
$Control->checkUser($_SESSION['user'],"prenom",htmlentities($_POST['fname']));
$Control->checkUser($_SESSION['user'],"tel",htmlentities($_POST['phone']));
$Control->checkUser($_SESSION['user'],"mail",htmlentities($_POST['mail']));
$Control->checkUser($_SESSION['user'],"birthdate",htmlentities($_POST['bdate']));
$Control->checkUser($_SESSION['user'],"mdp",hash('sha1',$_POST['password']));
$ControlA->checkAdress($_SESSION['adresse'],"NStreet",htmlentities($_POST['num_street']));
$ControlA->checkAdress($_SESSION['adresse'],"street",htmlentities($_POST['street']));
$ControlA->checkAdress($_SESSION['adresse'],"Postal",htmlentities($_POST['postal']));
$ControlA->checkAdress($_SESSION['adresse'],"City",htmlentities($_POST['city']));
$ControlA->checkAdress($_SESSION['adresse'],"Country",htmlentities($_POST['country']));
}

?>
<!--Compte-->
<div id=compte>
    <div class='flex'id="choix">
        <ul>
            <li ><a href='compte.php'>Voir les informations</a></li><br>
            <li ><a href='compte.php?p=par'>Modifier les informations</a></li><br>
            <li ><a href='compte.php?p=hist'>Historique des commandes</a></li><br>
            <?php if($_SESSION['user']->getRole()=='Admin'){
                    echo "<li ><a href='compte.php?p=add'>Ajouter/Supprimer des produits</a></li><br>";
                    echo "<li ><a href='compte.php?p=mod'>Modifier menu</a></li><br>";
            }?>
            <li ><a href="php/fonctions/logout.php">Déconnexion</a></li>
        </ul>
    </div>
<?php if(isset($_GET['p'])){
    // Modifier les parametres
      if($_GET['p']=='par'){
          ?>
    <div class='flex w-100' id='formbloc'>
        <div class='flex' id="formulaire">
            <form id="registerform" method="post">
                <div id='form1' class="flex">
                    <label for='name'>Nom :</label>
                    <input id='name' name='name' type=text required value="<?php echo $_SESSION['user']->getLastName()?>">
                    <label for='fname'>Prénom :</label>
                    <input id='fname' name='fname' type=text required value="<?php echo $_SESSION['user']->getFirstName()?>">
                    <label for='phone'>Téléphone :</label>
                    <input id='phone' name='phone' type=text required value="<?php echo $_SESSION['user']->getTel()?>">
                    <label for='mail'>E-mail :</label>
                    <input id='mail' name='mail' type=text onkeyup='regex("/^[a-z0-9\.]+@[a-z]+\.[a-z]{2,3}$/gm","mail")' required value="<?php echo $_SESSION['user']->getMail()?>">
                    <label for='bdate'>Date de naissance :</label>
                    <input id='bdate' name='bdate' type=date required value="<?php echo $_SESSION['user']->getBirthdate()?>">
                    <label for='password'>Mot de Passe :</label>
                    <input id='password' name='password' onkeyup='regex("/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,})$/gm","password")' type=password placeholder="Mot de passe">
                </div>
                <div id='form2'class="flex">
                    <label for='num_street'>Numéro de Rue :</label>
                    <input id='num_street' name='num_street' type=number required value="<?php echo ($_SESSION['adresse']->getStreetNumber())?>">
                    <label for='street'>Rue :</label>
                    <input id='street' name='street' type=text required value="<?php echo ($_SESSION['adresse']->getStreet())?>">
                    <label for='postal'>Code Postal :</label>
                    <input id='postal' name='postal' type=text required value="<?php echo ($_SESSION['adresse']->getPostalCode())?>">
                    <label for='city'>Ville :</label>
                    <input id='city' name='city' type=text required value="<?php echo ($_SESSION['adresse']->getCity())?>">
                    <label for='country'>Pays :</label>
                    <input id='country' name='country' type=country required value="<?php echo ($_SESSION['adresse']->getCountry())?>">
                    <label class='hidden' for='register'> Cliquer pour modifier  </label>
                    <input id='register' name='change' type=submit value="Modifier">
                </div>
            </form>
        </div>
        <div class='flex' id='regex'>
            <p id='regmail'></p><p id='regpassword'></p>
        </div>
    </div>

<?php
// Historique
}
if($_GET['p']=='hist'){
    $ControllerCommande=new ControllerCommande;
    $ControllerCommande->GetHisto();
    echo "<div id=histo>";
    $ancien="";
    foreach($ControllerCommande->getListe() as $Commande){
        echo "<div id=",$Commande->getNumero(),"><table border=1>
        <tr><td><b>Numéro de Commande : </b><br>",$Commande->getNumero(),"</td><td><b>Commandé le : </b><br>",$Commande->getDate(),"</td></tr>";
        foreach($Commande->getArticle() as $nom){
            echo "<tr ><td>",$nom,"</td><td>";
            foreach($controlart->getListe() as $article){
                $x=explode('-',$nom);
                if($article->getName()==$x[1]){
                    echo $x[0]*$article->getPrice(),"€</td></tr>";
                }
            }
        }
        echo "<tr><td><b> Total </b>: ",$Commande->getPrice(),"€</td></tr></table></div><br>";

    }
    echo "</div>";
?>

<?php
// Ajout de Produit
}
if($_GET['p']=='add'){
?>
<div id='addDel'>
    <div class='flex pc' id='addBloc'>
        <form action='./compte.php?p=add'id="uploadForm" method="post" enctype='multipart/form-data'>
            <label for="nomProduit">Nom du produit :</label>
            <input id='nomProduit' name='nomProduit' type=text required value="">
            <label for='Description'>Description :</label>
            <input id='description' name='description' type=text required value="">
            <label for='picture'accept="image/*">Image :</label>
            <input id='image' name='picture' type=file required value="">
            <label for='price'>Prix :</label>
            <input id='price' name='price' type=number step=0.01 required value="">
            <label for='type'>Catégorie :</label>
            <select id='type' name='type' form='uploadForm'>
                <option valeur="Entrée">Entrée</option>
                <option valeur="Plat">Plat</option>
                <option valeur="Dessert">Dessert</option>
                <option valeur="Boisson">Boisson</option>
            </select>
            <label for='addSub'class="hidden"> Upload </label>
            <input id='addSub' name='addSub' type='submit' onclick="document.location.href='./compte.php'" value='Ajouter !'>
        </form>
        <?php 

            if (isset($_POST['addSub'])){
                $pathfile="";
                if (isset($_FILES['picture']) AND $_FILES['picture']['error'] == 0){
                    // Testons si le fichier n'est pas trop gros
                    if ($_FILES['picture']['size'] <= 1000000){
                        // Testons si l'extension est autorisée
                        $infosfichier = pathinfo($_FILES['picture']['name']);
                        $extension_upload = $infosfichier['extension'];
                        $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png', 'xml');
                        if (in_array($extension_upload, $extensions_autorisees)){
                            //echo"Le fichier a la bonne extension<br/>";
                            // On peut valider le fichier et le stocker définitivement
                            $result = move_uploaded_file($_FILES['picture']['tmp_name'], './assets/plat_menu/'.basename($_FILES['picture']['name']));
                            if ($result == true) {
                                $pathfile='./assets/plat_menu/' . $_FILES['picture']['name'];                                
                            }
                            else {
                            echo "La sauvegarde a échouée" ;
                            };
                        }
                        else{
                                echo "Extension non attendue" ;
                        }
                    }
                }
                $controlart->AddArticle($_POST['nomProduit'],$_POST['description'],$pathfile,$_POST['price'],$_POST['type']);
            }
        ?>
    </div>
    <p class='mobile'> Cette fonctionnalité est indisponible sur Mobile !</p>
    <!--SUPPRESSION PRODUIT-->
    <div id='delprod' class='pc'>
        <div>
            <p> Suppression d'un produit </p>
            <select id='option'>
            <?php
                
                foreach($controlart->getListe() as $data) {
                    echo '<option value="' . $data->getName() . '"';
                    /*if ($_POST['delsub']==$data["name_product"]) {
                        echo " selected='selected'";
                    }*/
                    echo '>' . $data->getName() . '</option>';
                    }
                echo'</select><button type=submit name="delsub" onclick="delOption()">Effacer Article</button>';
                if(isset($_GET['art'])){
                    foreach($controlart->getListe() as $produit){
                        if ($produit->getName() == $_GET['art']){
                           $filename= $produit->getImage();
                            echo $filename;
                            if (file_exists($filename)){
                                echo $_GET['art'];
                                $controlart -> DelArticle($_GET['art']);
                                unlink($filename);
                            }
                            else {echo'NOT EXIST';}
                        }
                    }   
                }   
            ?>
        </div>
    </div>
</div>


<?php
}
if($_GET['p']=='mod'){
?>
<p>Ici on modifie des menus</p>

<?php
}}
else { 
    echo '<div id="showcompte"><h6>Prénom : </h6><p>'.$_SESSION['user']->getFirstName().
    '</p><h6> Nom : </h6><p>'.$_SESSION['user']->getLastName().
    '</p><h6> Date de Naissance : </h6><p>'.$_SESSION['user']->getBirthday().
    '</p><h6> E-Mail : </h6><p>'.$_SESSION['user']->getMail().
    '</p><h6> Téléphone : </h6><p>'.$_SESSION['user']->getTel().
    '</p><h6> Adresse : </h6><p>'.$_SESSION["adresse"]->getAdrresse().'</p>';
}
echo '</div>';
echo"<script>";    
require_once('js/regex.js');
echo"</script>";

 include_once('footer.php');
?>