<?php
class Article{
    private $idArticle;
    private $picture;
    private $nameArt;
    private $description;
    private $price;
    private $type;
    private $listeproduits;

    function __construct($idArticle,$picture,$nameArt,$description,$price,$listeproduits,$type){
        $this->_idArticle=$idArticle;
        $this->_picture=$picture;
        $this->_nameArt=$nameArt;
        $this->_description=$description;
        $this->_price=$price;
        $this->_listeproduits=$listeproduits;
        $this->_type=$type;
    }
    function getId(){
        return $this->_idArticle;
    }
    function getName(){
        return $this->_nameArt;
    }

    function getImage(){
        return $this->_picture;
    }
    function getType(){
        return $this->_type;
    }

    function getPrice(){
        return $this->_price;
    }
    function getDescription(){
        return $this->_description;
    }
}
?>