<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tortiplat / Home</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/index.css">
  <?php require_once('php/db_connect.php');?>
</head>
<?php 
include_once('header.php');
?>
<!--Contenu-->

<!--Carousel-->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators green">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src=".\assets\carousel\salade.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src=".\assets\carousel\banquet.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src=".\assets\carousel\saumon.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev green" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="sr-only">Previous</span>
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  </a>
  <a class="carousel-control-next green" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<!--Grid-->
<div class='mainGrid'>
  <div id='process' class='bg-img bandeau flexcenter'><img src='./assets/process.png'></div>
  <div id='newItem' class='bg-img flexcenter'><img src='./assets/newdish.png'></div>
  <div id='liencontact' class='bg-img bg-blk flexcenter'><img src='./assets/contactme.png'></div> 
  <div id='logo' class='bg-img bg-blk flexcenter'><img src='./assets/Logolong800w.png'></div>
  <div id='promos' class='bg-img flexcenter'><img src='./assets/promo.png'></div>
</div>
  
<?php
include_once('footer.php');
