package lab.util;

import java.awt.Toolkit;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.LineUnavailableException;

import javafx.application.Platform;
import javafx.scene.control.Label;
import lab.vue.LabyrintheController;

public class Timeur {
	private static int duree=60;
	private static int dur;
	static 
	int i=0;
	static Toolkit tk = Toolkit.getDefaultToolkit ();
	private int minute;
	private int seconde;
	
	public void timer(Label timeur) {
		if(duree==0) {
			duree=dur;			
		}
		dur=duree;
		Timer tm = new Timer();
		tm.schedule(new TimerTask(){
			@Override
			public void run() { 
				Platform.runLater(() -> timeur.setText("Temps : "+i+" Secondes"));
			}
		}, 0);
		Timer tr = new Timer() ;
		tr.scheduleAtFixedRate(new TimerTask(){
			@Override
			public void run(){
				if(i<duree) {
					System.out.println("Temps : "+i+" Secondes");
					Platform.runLater(() -> timeur.setText("Temps : "+i+" Secondes"));
					i+=1;
				}
				else {
					try {
						Sound.tone(500,500);
					} catch (LineUnavailableException e) {
						e.printStackTrace();
					}
					tr.cancel();
					LabyrintheController.gamestop=true;
					i=0;
				}
			}
		}, 1000, 1000);
	}
	
	public static void SetDuree(int dure) {
		duree=dure;
	}
	
	public static void Stop() {
		duree=0;
	}
	
	public int getScore() {
		return i;
	}
	
	public Time getTime() {
        Time time = null;
        if(i<60) {
        	minute=0;
        	seconde=i;
        	time =Time.valueOf(LocalTime.of(0,minute,seconde));
        }
        else if(i>60) {
        	seconde=i%60;
        	minute=(i-seconde)/60;
        	time =Time.valueOf(LocalTime.of(0,minute,seconde));
        }
		return time;
	}
}