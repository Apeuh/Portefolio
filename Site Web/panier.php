<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tortiplat / Panier</title>
  <link rel="stylesheet" href="./css/login.css">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/panier.css">
  <?php require_once('php/db_connect.php');
    include_once('header.php');
    include("php/controller/ControllerMenu.php");?>
</head>

<div id="mainBasket">
<script>
  //Supprimer un article
  function delArt(val){
    var xhttp = new XMLHttpRequest();
  url="php/fonctions/delArt.php?p="+val;
  xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              alert(xhttp.responseText);
              location.reload(true);
              };};
  xhttp.open("GET", url,true);
  xhttp.send();
  }
  //Quantity produit
  function modQuanti(val){
    var xhttp = new XMLHttpRequest();
    quantity=document.getElementById(val).value;
    url="php/fonctions/modQuanti.php?q="+quantity+"&p="+val;
    xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                location.reload(true);
                };};
    xhttp.open("GET", url,true);
    xhttp.send();
    }
</script>
<?php 
  $controllerMenu = new ControllerMenu;
  $control = new ControllerArticle;
  $control-> CreateArticle();
  $controllerMenu-> CreateMenu();
//CREATION DES ARTICLES DU PANIER//
//session_destroy();
  if(isset($_SESSION["Panier"]) && !empty($_SESSION["Panier"])){
    $obj = '';
    $total = 0;
    // RECUPERATION DATA MENU //
    foreach($_SESSION["Panier"] as $key=>$val){
      if (substr($key,0,4)=='Menu'){
        $expl=explode("|",$key);
        foreach($controllerMenu->getListe() as $menu)
        if ($menu->getId()==$expl[1]){
    // GENERATION CARTE MENU AJOUTER AU PANIER //      
          echo "<div class='itemcard itemMenu'>
          <img class='itemimgMenu' src='./assets/plat_menu/Menu.jpg'>
          <span><b>Menu :".$menu->getName()."</b></span>
          <span> Composé de : <br>".$expl[2]."+".$expl[3]."+".$expl[4]."</span>
          <div>
            <span>Quantité : ",$val,"</span><br>
            <span> Prix :",$menu->getPrice()*$val,"€</span>";
            echo "<img src='./assets/bin.png' class='delArt' onclick='delArt(&quot;".$key."&quot;)'></div><br></div><br>";        
            
          $total+=($menu->getPrice()*$val);
        }
      }
      // RECUPERATION DATA ARTICLE //
      else {
        foreach($control->getListe() as $value){
          if ($value->getName()==$key){
            $obj=$value;
          }
        }
        echo "<div class='itemcard'><img class='itemimg' src='",$obj->getimage(),"'>","<span>$key</span>"," <select id='article".$obj->getId()."' onchange=modQuanti('article".$obj->getId()."')> ";
        // GESTION QUANTITE ARTICLE //  
        for($i=1;$i<40;$i++){
            if($i==$val){
              echo "<option value=$i selected>$i</option>";
            }
            else {echo "<option value=$i>$i</option>";};
          }
          // AFFICHAGE PRIX DYNAMIQUE //
          foreach($control->getListe() as $article){
            if($article->getName()==$key){
              echo "</select><span>",($article->getPrice())*$val,'€</span>';
              $total+=($article->getPrice())*$val;
            }
          }
          //  BOUTON SUPPRESSION ARTICLE //
          echo "<img src='./assets/bin.png' class='delArt' onclick=delArt('article".$obj->getId()."')></div><br>";
        }
      } 
      // AFFICHAGE DYNAMIQUE DU TOTAL DU PANIER //
      echo "<div id='Total'>Total TTC : $total €</div>"

      // BOUTON VIDER LE PANIER //
?>
  <div id="buttonbox">
  <a class="button" id="delbutton" href="php/fonctions/delpanier.php"><div>Vider le panier</div></a>
    <?php 
    if(isset($_SESSION['user'])){
      echo '<a class="button" id="confirm" href="checkout.php?"><div>Valider le panier</div></a>';
    }
    else echo  '<a class="button" id="connectbutton" href="login.php"><div>Connectez vous pour commander</div></a>'; $_SESSION['total']=$total;
    ?>
  </div>
</div>

<?php
}
else echo "</div><p id='empty'>Le panier est vide.</p>";
 include_once('footer.php');
?>