from dataclasses import dataclass

import  pygame, pyscroll, pytmx

from src.character import NPC


@dataclass
class Portal:
    from_world : str
    enter_point:str
    target_world:str
    teleport_point:str

@dataclass
class Map :
    name : str
    walls : list[pygame.Rect]
    group : pyscroll.PyscrollGroup
    tmx_data : pytmx.TiledMap
    portals:list[Portal]
    npcs:list[NPC]

class MapManager:
    def __init__(self, screen, player):
        self.maps=dict()
        self.current_map = "world"
        self.screen=screen
        self.player=player

        self.register_map("world",portals=[
            Portal(from_world="world", enter_point="door", target_world="house", teleport_point="spawn_house"),
            Portal(from_world="world", enter_point="enter_dungeon", target_world="dungeon", teleport_point="spawn_dungeon"),
            Portal(from_world="world", enter_point="hidden_door", target_world="house2", teleport_point="spawn_house2"),]
                          , npcs=[
            NPC("paul", nb_point=4, dialog=["Salut, moi c'est Zicco","Une preuve?","*pete sur un micro*"])
        ])
        self.register_map("house", portals=[
            Portal(from_world="house", enter_point="house_exit", target_world="world", teleport_point="spawn_world"),
            Portal(from_world="house", enter_point="lvl0", target_world="basement", teleport_point="spawn_lvl1-0"),
            Portal(from_world="house", enter_point="lvl2", target_world="bedroom", teleport_point="spawn_bedroom")]
        )
        self.register_map("basement", portals=[
            Portal(from_world="basement", enter_point="lvl1", target_world="house", teleport_point="spawn_lvl0-1")])
        self.register_map("dungeon", portals=[
            Portal(from_world="dungeon", enter_point="exit_dungeon", target_world="world", teleport_point="exit_dungeon_spawn")],npcs=[
            NPC("boss",nb_point=4,dialog=["Arrête, arrête c'est nwar ! *emoji drapeau pirate*"])]
        )
        self.register_map("house2", portals=[
            Portal(from_world="house2", enter_point="exit_house2", target_world="world", teleport_point="spawn_world2")])
        self.register_map("bedroom", portals=[
            Portal(from_world="bedroom", enter_point="exit_bedroom", target_world="house", teleport_point="spawn_lvl2-1")])

        self.teleport_player("spawn_world")
        self.tp_npcs()

    def check_npc_collision(self,dialog_box):
        for sprite in self.get_group().sprites():
            if sprite.feet.colliderect(self.player.rect) and type(sprite) is NPC:
                dialog_box.execute(sprite.dialog)

    def check_collision(self):
        #portails
        for portal in self.get_map().portals:
            if portal.from_world==self.current_map:
                point= self.get_object(portal.enter_point)
                rect=pygame.Rect(point.x,point.y,point.width,point.height)
                if self.player.feet.colliderect(rect):
                    copy_portal=portal
                    self.current_map=portal.target_world
                    self.teleport_player(copy_portal.teleport_point)
        #verification de la collision avec environnement
        for sprite in self.get_group().sprites():
            if type(sprite) is NPC:
                if sprite.feet.colliderect(self.player.rect):
                    sprite.speed=0
                else :
                    sprite.speed=2
            if (sprite.feet.collidelist(self.get_walls())) > -1:
                sprite.move_back()


    def teleport_player(self,name):
        point = self.get_object(name)
        self.player.position[0]= point.x
        self.player.position[1] = point.y
        self.player.save_location()


    def register_map(self, name, portals=[],npcs=[]):
        # Charger la carte en tmx
        tmx_data = pytmx.util_pygame.load_pygame(f"./map/{name}.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 2.5

        # defenir liste qui stock collision rectangle
        walls = []

        for obj in tmx_data.objects:
            if obj.type == "collision":
                walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # Dessiner le groupe de calque
        group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=7)
        group.add(self.player)

        #recuperer et ajouter npc
        for npc in npcs:
            group.add(npc)

        #Créer un objet map
        self.maps[name]=Map(name,walls,group,tmx_data,portals,npcs)

    def get_map(self) : return self.maps[self.current_map]

    def get_group(self): return self.get_map().group

    def get_walls(self): return self.get_map().walls

    def get_object(self,name): return self.get_map().tmx_data.get_object_by_name(name)

    def tp_npcs(self):
        for map in self.maps:
            map_data=self.maps[map]
            npcs=map_data.npcs

            for npc in npcs:
                npc.load_point(map_data.tmx_data)
                npc.spawn_npc()

    def draw(self):
        self.get_group().draw(self.screen)
        self.get_group().center(self.player.rect.center)

    def update(self):
        self.get_group().update()
        self.check_collision()
        for npc in self.get_map().npcs:
            npc.move()