package lab.classe;
import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
public class Maze {
	private int rows;
	private int columns;
	private int blockSize;
	Color[] color;
	GraphicsContext g;
	public Maze(int rows, int columns, int blockSize) {
		this.rows=rows;
		this.columns=columns;
		this.blockSize=blockSize;
}
	
	public void setRows(int rows) {
		this.rows=rows;
	}
	public void setColumns(int columns) {
		this.columns= columns;
	}
	public void setblockSize(int blockSize) {
		this.blockSize=blockSize;
	}


	public int getRows() {
		return this.rows;
	}
	public int getBlockSize(){
		return this.blockSize;
	}
	public int getColumns(){
		return this.columns;
	}
	
}


