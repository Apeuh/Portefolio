package lab.modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import lab.classe.Partie;
import lab.controller.ControllerSoignant;
import lab.controller.ControllerUser;
import lab.util.dbConnection;

public class ModelePartie {
	
	dbConnection db = new dbConnection();
	
	public ResultSet LoadResult(String nom, String prenom) throws SQLException {
		 Connection connection= db.connect();
		 PreparedStatement stat =null;
		 ResultSet rs =null;
		 if(ControllerUser.Users.isEmpty()) {
			 if(nom.isEmpty() && prenom.isEmpty()) {
				 String sql = "SELECT concat(p.nom,\" \",p.prenom) as \"FullName\",j.date,j.difficulte,j.score FROM jeu j\r\n"
					 		+ "INNER JOIN patient p ON p.idPatient=j.idPatient";
						 stat=connection.prepareStatement(sql);
						 //stat.setString(1, login);
						 //stat.setString(2, password);
						 rs = stat.executeQuery();
			 }
			 else {
				 String sql = "SELECT concat(p.nom,\" \",p.prenom) as \"FullName\",j.date,j.difficulte,j.score FROM jeu j\r\n"
			 		+ "INNER JOIN patient p ON p.idPatient=j.idPatient where nom=? and prenom=?";
				 stat=connection.prepareStatement(sql);
				 stat.setString(1, nom);
				 stat.setString(2, prenom);
				 rs = stat.executeQuery();
			 }
		 }
		 else if(ControllerSoignant.Soignants.isEmpty()) {
			 String password = ControllerUser.Users.get(0).getPassword();
			 String sql;
			 if(password==null || password.isEmpty()) {
				 password=null;
				 sql = "SELECT concat(p.nom,\" \",p.prenom) as \"FullName\",j.date,j.difficulte,j.score FROM jeu j\r\n"
					 		+ "INNER JOIN patient p ON p.idPatient=j.idPatient where login=? and password is ?";
			 }
			 else {
				 sql = "SELECT concat(p.nom,\" \",p.prenom) as \"FullName\",j.date,j.difficulte,j.score FROM jeu j\r\n"
					 		+ "INNER JOIN patient p ON p.idPatient=j.idPatient where login=? and password=?";
			 }
			 		
					 stat=connection.prepareStatement(sql);
					 stat.setString(1, ControllerUser.Users.get(0).getLogin());
					 stat.setString(2, password);
					 rs = stat.executeQuery();
		 }
			 return rs;
	}
	
	public void addResult(Partie partie) throws SQLException {
		Connection connection= db.connect();
		 PreparedStatement stat =null;
		 String sql="INSERT INTO `jeu`(`score`, `date`, `difficulte`, `idPatient`) VALUES (?,?,?,?)";
		 Calendar calendar = new GregorianCalendar();
		 stat=connection.prepareStatement(sql);
		 stat.setTime(1, partie.getScore());
		 stat.setTimestamp(2, partie.getDate());
		 stat.setString(3, partie.getDifficulte());
		 stat.setInt(4, partie.getId());
		 stat.executeUpdate();
	}

}
