package lab.modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import lab.controller.ControllerSoignant;
import lab.controller.ControllerUser;
import lab.util.dbConnection;

public class ModeleUser {

	private static dbConnection db = new dbConnection();
	
	public ResultSet Login(String login, String password) throws SQLException {
		 Connection connection= db.connect();
		 PreparedStatement stat =null;
		 ResultSet rs =null;
		 String sql=null;
		 if(password.isEmpty()) {
			 password=null;
			 sql = "SELECT * FROM patient WHERE login = ? AND password is ?";
		 }
		 else {
			 sql = "SELECT * FROM patient WHERE login = ? AND password = ?";
		 }
			 stat=connection.prepareStatement(sql);
			 stat.setString(1, login);
			 stat.setString(2, password);
			 rs = stat.executeQuery();
			 return rs;
			 }
	
	public void Inscription(String nom, String prenom, String login, String password) throws SQLException {
		 Connection connection= db.connect();
		 PreparedStatement stat =null;
		 String sql = " INSERT INTO patient(`nom`, `prenom`, `login`, `password`) VALUES (?,?,?,?)";
		 stat=connection.prepareStatement(sql);
		 stat.setString(1, nom);
		 stat.setString(2, prenom);
		 stat.setString(3, login);
		 stat.setString(4, password);
		 stat.executeUpdate();
	}
	
	public void Changement(String nom, String prenom, String password) throws SQLException {
		 Connection connection= db.connect();
		 PreparedStatement stat =null;
		 String sql="UPDATE patient SET `nom`=?,`prenom`=?,login =?, `password`=? WHERE login=?";
		 stat=connection.prepareStatement(sql);
		 String login=ControllerUser.Users.get(0).getLogin();
		 String NewLogin=prenom+"."+nom;
		 stat.setString(1, nom);
		 stat.setString(2, prenom);
		 stat.setString(3, NewLogin.toLowerCase());
		 stat.setString(4, password);
		 stat.setString(5, login);
		 stat.executeUpdate();
	}
}

